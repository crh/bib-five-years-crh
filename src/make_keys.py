import os
import re

def make_keys(path) -> None:
    """
    Replacing  keys' numbers by new numbers from 1.
    :param path: path to a BibLaTeX file
    :type path: str
    """
    with open(path, 'r', encoding='utf-8') as reading:
        print("**** BEGINNING ANALYSING {} ****".format(path))
        file = reading.read()
        key_name_year = r"@\w+{[a-z]+[0-9]{4}"  # @entry{author2020
        # 0/ replacing ’ by ', & by \&
        file = file.replace("’", "'")
        file = file.replace(" &", " \&")
        corrections = ["fauve-chamoux", "velay-vallantin", "klapisch-zueber",
                       "alexandre-bidon", "ducreux-lakits", "ozouf-marignier",
                       "marie-vic", "powell-nagata", "paul-andr", "bonnain-dulon",
                       "postel-vinay", "martin-fugier", "froeschle-chopard",
                       "audouin-rouzeau", "geoffroy-poisson", "klapisch-zuber",
                       "dauphin-memeteau", "jean-claude"]
        for correction in corrections:
            file = file.replace(correction, correction.replace("-", ""))
        # 1/ removing old keys numbers
        file = re.sub(r"(" + key_name_year + r")-\d+[a-z]?,", r"\1,", file)
        # 2/ detecting keys
        key_plus_lines = re.findall(r"((" + key_name_year + r")(.+\n.+\n.+))", file)
        print("**** {} KEYS DETECTED ****".format(len(key_plus_lines)))
        # 3/ renumbering
        counter = 0
        for item in key_plus_lines:
            counter += 1
            print("Key n°{} : {}".format(counter, item[1]))
            new_key = item[0].replace(item[1], item[1] + "-" + str(counter))
            file = file.replace(item[0], new_key)
    with open(path, 'w', encoding='utf-8') as writting:
        writting.write(file)
        print("**** DONE ****")


make_keys("../data/biblio_1998_2001/biblio19982001.bib")
make_keys("../data/biblio_2000_2004/biblio20002004.bib")
make_keys("../data/biblio_2004_2008/biblio20042008.bib")
make_keys("../data/biblio_2010_2013/biblio20102013.bib")
make_keys("../data/biblio_2012_2016/biblio20122016.bib")
make_keys("../data/biblio_2017_2022/main_2017_2022.bib")
